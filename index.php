<?php
/**
 * @version  php v.7.2
 * @version  PROGRAM_TITLE v.1.1.0
 * @api      https://www.amocrm.ru/developers/content/crm_platform/platform-abilities
 * @license  COMMERCIAL USE, NON-FREE, SET YOUR LICENSE HERE
 * @package  RUBIKON DEVELOPMENT
 * @author   Alejandro A. Shevyakov aka "Feanor Moriell" <sindyjay@yandex.ru> @ Limberia Development Group (LDGroup)
 * @link     https://vk.com/id4907651
 */
//set_time_limit(79200);// 22 часа // устанавливать только для скриптов с длительным сроком выполнения

const AMO_NAME           = "";
const AMO_SECRET_KEY     = "";
const AMO_INTEGRATION_ID = "";
const AMO_AUTH_CODE      = "";
const AMO_REDIRECT_URI   = "https://SUBDOMAIN.amocrm.ru/";


require_once 'src/dependencies.php';

use AmoCRM\YourAmoIntegrationName;
use ldgroup\Logger;

try {
    $__API = new YourAmoIntegrationName(
        AMO_NAME,
        AMO_INTEGRATION_ID,
        AMO_SECRET_KEY,
        AMO_AUTH_CODE,
        AMO_REDIRECT_URI
    );
    (new Logger($__API->TestGetLeads()))->Write();
}
catch (Error | Exception $e)
{
    (new Logger($e))->Write()->SaveJson();
    die();
}
