<?php


namespace ldgroup;


interface ILogger
{

    public function SetFilename(string $filename): Logger;

    public function Write(): Logger;



    public function SaveRaw(): void;

    public function SaveJson(): void;

}