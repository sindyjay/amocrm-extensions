<?php


namespace ldgroup;


class Logger implements ILogger
{

    private const DEFAULT_FILENAME = 'src/log/log';
    private const DELIMITER_SYMBOL = '-';
    private const DELIMITER_REPEAT = 80;

    private $filename          = self::DEFAULT_FILENAME;
    private $isDelimiterNeedle = false;
    private $logData;



    public function __construct($logData)
    {
        $this->_setLogData($logData);
        $this->_setFilename(self::DEFAULT_FILENAME);
    }


    public static function FormatException($info): array {
        return [
            "message"        => $info->getMessage() ?? "EXCEPTION OCCURRED",
            "code"           => $info->getCode()    ?? 0,
            "file"           => $info->getFile()    ?? "unknown file",
            "line"           => $info->getLine()    ?? 0,
            "stackTrace"     => $info->getTrace()   ?? []
        ];
    }



    final public function SetFilename(string $filename): Logger
    {
        $this->_setFilename(
            (trim($filename) !== '')
                ? trim($filename)
                : self::DEFAULT_FILENAME
        );
        return $this;
    }

    final public function Write(bool $isDelimiterNeedle = false): Logger
    {
        $this->_setIsDelimiterNeedle($isDelimiterNeedle);
        print $this->getFormattedRaw();
        return $this;
    }

    final public function SaveRaw(bool $isDelimiterNeedle = false): void
    {
        $this->_setIsDelimiterNeedle($isDelimiterNeedle);
        $this->saveLog($this->getFormattedRaw());
    }

    final public function SaveJson(): void
    {
        $this->saveLog(sprintf(
            '%s%s',
            json_encode($this->_getLogData(), JSON_UNESCAPED_UNICODE|JSON_FORCE_OBJECT|JSON_UNESCAPED_SLASHES),
            PHP_EOL
        ), true);
    }



    private function getDelimiter(): string {
        return str_repeat(self::DELIMITER_SYMBOL, self::DELIMITER_REPEAT);
    }

    private function getFormattedRaw(): string {
        return sprintf(
            '%s%s%s%s%s',
            print_r($this->_getLogData(), true),
            PHP_EOL,
            PHP_EOL,
            ($this->_getIsDelimiterNeedle() === true) ? $this->getDelimiter() : '',
            PHP_EOL
        );
    }

    private function saveLog(string $logData, bool $isJson = false): void {
        $extension = ($isJson === true) ? '.json' : '.txt';
        file_put_contents(
            $this->_getFilename() . $extension,
            $logData,
            FILE_APPEND
        );
    }



    private function _setFilename(string $filename): void {
        $this->filename = $filename;
    }

    private function _getFilename(): string {
        return (!is_null($this->filename)) ? $this->filename : self::DEFAULT_FILENAME;
    }

    private function _setLogData($logData): void {
        $this->logData = $logData;
    }
    private function _getLogData() {
        return $this->logData;
    }

    private function _setIsDelimiterNeedle(bool $isDelimiterNeedle): void {
        $this->isDelimiterNeedle = $isDelimiterNeedle;
    }
    private function _getIsDelimiterNeedle(): bool {
        return $this->isDelimiterNeedle;
    }
}