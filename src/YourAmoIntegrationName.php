<?php

namespace AmoCRM;


class YourAmoIntegrationName extends V4Connector
{

    /**
     * TODO: remove this function after the first successful test connection
     * @return array
     */
    final public function TestGetLeads(): array
    {
        return $this->GetCurlResponseData($this->APIGet("leads"));
    }
}
